#!/usr/bin/env python3
import typing
import re
from hashlib import sha256
from pathlib import Path, PurePath
from pprint import pprint

import sh
import sh.contrib
from ClassDictMeta import ClassDictMeta
from fsutilz import copytree, movetree

from prebuilder import globalPrefs
from prebuilder.buildPipeline import BuildPipeline, BuildRecipy
from prebuilder.core.Package import PackageInstalledFiles, PackageRef
from prebuilder.core.RunConfig import RunConfig
from prebuilder.distros.debian import Debian
from prebuilder.distros.debian.utils import debianPackageRelationKwargs
from prebuilder.fetchers import DiscoverDownloadVerifyUnpackFetcher, GitRepoFetcher
from prebuilder.fetchers.discoverers import GithubReleasesDiscoverer
from prebuilder.fetchers.unpackers import GZIP, Archive, Tar
from prebuilder.fetchers.verifiers import GPGVerifier, HashsesVerifier
from prebuilder.importers.debhelper import parseDebhelperDebianDir
from prebuilder.repoPipeline import RepoPipelineMeta
from prebuilder.systems.dummy import Dummy, DummyBS
from prebuilder.tools import getCfj

ourCMakePrefix = "vanilla-cmake"


class CMakeRepackageBS(DummyBS):
	def __call__(self, sourceDir, ref: PackageRef, packagesRootsDir: Path, buildOptions, gnuDirs, firejailCfg=None, candidateDirRx=None):
		results = {}
		shareDataDir = findCMakeDataDir(sourceDir, candidateDirRx)
		usr = sourceDir / "usr"

		for d in ("bin", "share"):
			movetree(sourceDir / d, usr / d)

		usrShare = usr / "share"

		for d in ("man", "doc"):
			movetree(sourceDir / d, usrShare / d)

		print("shareDataDir", shareDataDir)
		# raise Exception()
		return super().__call__(sourceDir, ref=ref, packagesRootsDir=packagesRootsDir, buildOptions=buildOptions, gnuDirs=gnuDirs, firejailCfg=firejailCfg)


CMakeRepackage = CMakeRepackageBS()


def findCMakeDataDir(cmakeUnpackedRoot, candidateDirRx):
	cmakeDataDir = None
	for cand in (cmakeUnpackedRoot / "share").iterdir():
		# print(cand, cand.is_dir(), candidateDirRx.match(cand.name))
		if cand.is_dir():
			if candidateDirRx.match(cand.name):
				cmakeDataDir = cand
				break
	return cmakeDataDir


class build(metaclass=RepoPipelineMeta):
	"""{maintainerName}'s repo for apt with CMake binary packages, built from the official builds on GitHub"""

	DISTROS = (Debian,)

	def cmake():
		thisDir = Path(".")
		packagesRootsDir = thisDir / "packagesRoots"

		runConfig = RunConfig()
		debianCmakeDir = thisDir / "debian_cmake"
		fetcher = GitRepoFetcher("https://salsa.debian.org/cmake-team/cmake.git")
		fetcher(debianCmakeDir, runConfig)

		hashFuncName = "SHA-256"
		versionRxText = "(?:\\d+\\.){1,2}\\d+(?:-rc\\d+)?"
		platformMarker = "Linux-x86_64"

		candidateDirRx = re.compile("^cmake-" + versionRxText + "$")
		licenseFileURI = "https://gitlab.kitware.com/cmake/cmake/raw/master/Copyright.txt"

		downloadFileNameRx = re.compile("^" + "-".join(("cmake", versionRxText, platformMarker)) + "\\.tar\\.gz$")
		hashesFileNameRxText = "-".join(("cmake", versionRxText, hashFuncName)) + "\\.txt"
		hashesSigFileNameRxText = hashesFileNameRxText + "\\.(?:asc|sig|gpg)"

		fetcher = DiscoverDownloadVerifyUnpackFetcher(
			GithubReleasesDiscoverer(
				"Kitware/CMake", tagRx=re.compile("^v(" + versionRxText + ")$"),
				downloadFileNamesRxs={
					"binary.tgz": downloadFileNameRx,
					"hashes": re.compile("^" + hashesFileNameRxText + "$"),
					"hashes.asc": re.compile("^" + hashesSigFileNameRxText + "$"),
				},
				titleRx = None
			),
			HashsesVerifier(GPGVerifier, sha256, hashesFileName="hashes",  signatureFileName="hashes.asc", keyFingerprint = "CBA23971357C2E6590D9EFD3EC8FEF3A7BFB4EDA", keyFile=thisDir / "brad.gpg"),
			Archive("binary.tgz", Tar, GZIP)
		)
		buildRecipy = BuildRecipy(CMakeRepackageBS(), fetcher, candidateDirRx=candidateDirRx, subdir=lambda srcDir, ref: srcDir / ("cmake-" + ref.version + "-" + platformMarker))

		res = parseDebhelperDebianDir(debianCmakeDir / "debian")
		for metadata, tearingSpec in res["pkgs"]:
			metadata.ref.name = metadata.ref.name.replace("cmake", ourCMakePrefix)
			for relName in debianPackageRelationKwargs:
				if relName in metadata.controlDict:
					for r in metadata.controlDict[relName]:
						r.name = r.name.replace("cmake", ourCMakePrefix)

		#res["pkgs"] = [p for p in res["pkgs"] if (p[0].ref.name == "vanilla-cmake" and p[0].ref.group == "data")]
		#print(res["pkgs"])
		return BuildPipeline(buildRecipy, ((Debian, res["pkgs"]),))


if __name__ == "__main__":
	build()
